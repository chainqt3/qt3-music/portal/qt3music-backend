package com.example.qt3musicbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication
public class Qt3musicBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(Qt3musicBackendApplication.class, args);
	}

}
